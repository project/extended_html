<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.1  2010/08/15 21:39:36  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//
// Revision 1.1.1.1  2010/08/15 21:17:29  maulikkamdar
// Initial Import
//
/**
 * Implements hook_form_alter().
 */
function extended_html_uploads_form_node_type_form_alter(&$form, $form_state) {
  if (count($widgets = _extended_html_uploads_widgets($form['#node_type']->type)) > 0) {
    $form['extended_html_uploads'] = array(
      '#type' => 'fieldset',
      '#title' => t("Drag & Drop Uploads settings"),
      '#group' => 'additional_settings',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['extended_html_uploads']['extended_html_uploads_widget'] = array(
      '#type' => 'select',
      '#title' => t('Upload widget'),
      '#options' => array('' => '') + $widgets,
      '#default_value' => array_key_exists(variable_get('extended_html_uploads_widget_' . $form['#node_type']->type, 0), $widgets)
        ? variable_get('extended_html_uploads_widget_' . $form['#node_type']->type, 0) : 0,
      '#ajax' => array(
        'callback' => 'extended_html_uploads_js',
        'wrapper' => 'edit-extended-html-uploads-wrapper',
      ),
      '#description' => t("Leave blank to disable Drag & Drop Uploads functionality."),
      '#prefix' => '<div id="edit-extended_html-uploads-wrapper">',
    );
    $widget = variable_get('extended_html_uploads_widget_' . $form['#node_type']->type, 0) !== 0
      ? unserialize(variable_get('extended_html_uploads_widget_' . $form['#node_type']->type, 0)) : NULL;
    if (!function_exists('field_ui_formatter_options')) {
      module_load_include('inc', 'field_ui', 'field_ui.admin');
    }
    $formatter_options = !is_null($widget['module']) ? field_ui_formatter_options($widget['module']) : array();
    $form['extended_html_uploads']['extended_html_uploads_formatter'] = array(
      '#type' => 'select',
      '#title' => t('Output style'),
      '#options' => $formatter_options,
      '#default_value' => array_key_exists(variable_get('extended_html_uploads_formatter_' . $form['#node_type']->type, 0), $formatter_options)
        ? variable_get('extended_html_uploads_formatter_' . $form['#node_type']->type, 0) : 0,
      '#disabled' => count($formatter_options) == 0,
      '#suffix' => '</div>',
    );
    $form['extended_html_uploads']['extended_html_uploads_hide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide upload widget?'),
      '#default_value' => variable_get('extended_html_uploads_hide_' . $form['#node_type']->type, 0),
    );
    $form['#submit'][] = 'extended_html_uploads_node_type_form_submit';
  }
}

function extended_html_uploads_form_alter(&$form, $form_state, $form_id) {
  if (strstr($form_id, '_node_form') && count($widgets = _extended_html_uploads_widgets($form['#node']->type)) > 0
      && ($data = variable_get('extended_html_uploads_widget_' . $form['#node']->type, '')) !== '') {
    drupal_add_js(drupal_get_path('module', 'extended_html_uploads') . '/misc/extended_html.uploads.js');
    drupal_add_css(drupal_get_path('module', 'extended_html_uploads') . '/misc/extended_html.uploads.css');
    $form['extended_html_uploads'] = array('#markup' => '<div id="extended-html-uploads"></div>');
    $form['extended_html_uploads_progress'] = array('#markup' => theme('progress_bar', array('percent' => 0, 'message' => 'Uploading file')));
    drupal_add_js(array('ExtendedHTMLUploads' => array(
      'hide' => variable_get('extended_html_uploads_hide_' . $form['#node']->type, 0), 'target' => NULL, 'trigger' => FALSE
    ) + _extended_html_uploads_js_data($widgets, unserialize($data))), 'setting');
  }
}

function _extended_html_uploads_widgets($type) {
  $widgets = array();
  foreach (module_implements('extended_html_uploads_widgets') as $module) {
    $function = $module . '_extended_html_uploads_widgets';
    $function($type, $widgets);
  }
  return $widgets;
}

function _extended_html_uploads_js_data($widgets, $selected) {
  $data = array();
  foreach ($widgets as $widget => $label) {
    $widget = unserialize($widget);
    $function = $widget['module'] . '_extended_html_uploads_js_data';
    if (function_exists($function)) {
      $widget_data = $function($widget);
      $data += $widget_data;
    }
    if ($selected == $widget) {
      $selected_data = array_values($widget_data);
    }
  }

  return array('dropzones' => array(
    'default' => array('id' => 'default', 'selector' => 'textarea', 'target' => TRUE) + $selected_data[0]
    ) + $data
  );
}

function extended_html_uploads_js($form, $form_state) {
  $null = theme('status_messages');
  return drupal_render($form['extended_html_uploads']['extended_html_uploads_widget']) . drupal_render($form['extended_html_uploads']['extended_html_uploads_formatter']);
}

<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.3  2010/08/16 15:55:31  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//
// Revision 1.2  2010/08/16 15:47:01  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//

/**
 * Implement hook_element_info().
 */
function extended_html_element_info() {
  $types['teltext'] = array(
     '#input' => TRUE,
  );
  $types['urltext'] = array(
    '#input' => TRUE,
    '#size' => 80,
    '#maxlength' => 128,
   );
  $types['emailtext'] = array(
    '#input' => TRUE,
    '#size' => 60,
    '#maxlength' => 128,
  );
  $types['searchtext'] = array(
    '#input' => TRUE,
    '#size' => 60,
    '#maxlength' => 255,
  );
  $types['rangefield'] = array(
    '#input' => TRUE,
    '#min' => '',
    '#max' => '',
  );

   return $types;
 }
 

/**
 * Implement hook_theme().
 */
function extended_html_theme() {
   return array(
    'teltext' => array(
      'arguments' => array('element' => NULL),
	  'page callback' => 'theme_teltext',
	  'file' => 'extended_html.theme.inc',
    ),
    'urltext' => array(
      'arguments' => array('element' => NULL),
	  'page callback' => 'theme_urltext',
	  'file' => 'extended_html.theme.inc',
    ),
    'emailtext' => array(
       'arguments' => array('element' => NULL),
	   'page callback' => 'theme_emailtext',
	   'file' => 'extended_html.theme.inc',
    ),
    'searchtext' => array(
      'arguments' => array('element' => NULL),
	  'page callback' => 'theme_searchtext',
	  'file' => 'extended_html.theme.inc',
    ),
    'rangefield' => array(
      'arguments' => array('element' => NULL),
	  'page callback' => 'theme_rangefield',
	  'file' => 'extended_html.theme.inc',
    ),
   );
 }
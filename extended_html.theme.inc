<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.3  2010/08/16 15:55:31  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//
// Revision 1.2  2010/08/16 15:47:01  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//

/**
 * @file
 * Theming functions for various elements of HTML 5
 * Returns HTML string for the new HTML 5 elements
 */
 
function theme_video($variables) {
  $path = $variables['path'];
  $alt = $variables['alt'];
  $title = $variables['title'];
  $attributes = $variables['attributes'];
  $getsize = $variables['getsize'];

  if ($getsize) {
    $attributes = drupal_attributes($attributes);
    $url = file_create_url($path);
    return '<video src="' . check_url($url) . '" alt="' . check_plain($alt) . '" title="' . check_plain($title) . '" ' . $attributes . ' />';
  }
}

function theme_teltext($variables) {
  $size = empty($variables['#size']) ? '' : ' size="'. $variables['#size'] .'"';
  $maxlength = empty($variables['#maxlength']) ? '' : ' maxlength="'. $variables['#maxlength'] .'"';
  $required = empty($variables['#required']) ? '' : ' required';
  $class = array('form-telephone');
  $extra = '';

  _form_set_class($variables, $class);

  return '<input type="tel"'. $maxlength . $required . ' name="'. $variables['#name'] .'" id="'. $variables['#id'] .'"'. $size .' value="'. check_plain($variables['#value']) .'"'. drupal_attributes($variables['#attributes']) .' />';
}

function theme_urltext($variables) {
  $size = !empty($variables['#size']) ?  ' size="'. $variables['#size'] .'"' : '';
  $maxlength = !empty($variables['#maxlength']) ? ' maxlength="'. $variables['#maxlength'] .'"' : '';
  $required = !empty($variables['#required']) ? ' required' : '';
  $class = array('form-url');
  $extra = '';

  _form_set_class($variables, $class);

  return '<input type="url"'. $maxlength . $required . ' name="'. $variables['#name'] .'" id="'. $variables['#id'] .'"'. $size .' value="'. check_plain($variables['#value']) .'"'. drupal_attributes($variables['#attributes']) .' />';
}

function theme_emailtext($variables) {
  $size = empty($variables['#size']) ? '' : ' size="'. $variables['#size'] .'"';
  $maxlength = empty($variables['#maxlength']) ? '' : ' maxlength="'. $variables['#maxlength'] .'"';
  $class = array('form-email');
  
  _form_set_class($variables, $class);

  return '<input type="email"'. $maxlength . $required . ' name="'. $variables['#name'] .'" id="'. $variables['#id'] .'"'. $size .' value="'. check_plain($variables['#value']) .'"'. drupal_attributes($variables['#attributes']) .' />';
}

function theme_searchtext($variables) {
  $size = empty($variables['#size']) ? '' : ' size="'. $variables['#size'] .'"';
  $maxlength = empty($variables['#maxlength']) ? '' : ' maxlength="'. $variables['#maxlength'] .'"';
  $required = empty($variables['#required']) ? '' : ' required';
  $class = array('form-search');

  _form_set_class($variables, $class);
  
  return '<input type="search"'. $maxlength . $required . ' name="'. $variables['#name'] .'" id="'. $variables['#id'] .'"'. $size .' value="'. check_plain($variables['#value']) .'"'. drupal_attributes($variables['#attributes']) .' />';
}

function theme_rangefield($variables) {
  $min = empty($variables['#min']) ? '' : ' min="'. $variables['#min'] .'"';
  $max = empty($variables['#max']) ? '' : ' max="'. $variables['#max'] .'"';
  $class = array('form-range');

  _form_set_class($variables, $class);

  return '<input type="range"'. $min .' name="'. $variables['#name'] .'" id="'. $variables['#id'] .'"'. $max .' value="'. check_plain($variables['#value']) .'"'. drupal_attributes($variables['#attributes']) .' />';
}

function theme_audio($variables) {
  $path = $variables['path'];
  $alt = $variables['alt'];
  $title = $variables['title'];
  $attributes = $variables['attributes'];
  $attributes = drupal_attributes($attributes);
  $url = file_create_url($path);
  return '<audio src="' . check_url($url) . '" alt="' . check_plain($alt) . '" title="' . check_plain($title) . '" ' . $attributes . ' />';
}

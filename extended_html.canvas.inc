<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.1  2010/08/15 21:36:07  maulikkamdar
// Initial commit of extended_html module. Contains theming functions for new HTML 5 elements, and integration of Drag and Drop API.
//
// Revision 1.1.1.1  2010/08/15 21:17:29  maulikkamdar
// Initial Import
//

function extended_html_canvas_js() {
	drupal_add_js(drupal_get_path('module', 'extended_html').'/misc/extended_html.excanvas.compiled.js');
	drupal_set_html_head('<!--[if IE]><script type="text/javascript" src="'.drupal_get_path('module', 'extended_html').'/misc/extended_html.excanvas.compiled.js"></script><![endif]-->');
}